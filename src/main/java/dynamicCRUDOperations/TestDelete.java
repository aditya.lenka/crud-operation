package dynamicCRUDOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestDelete {

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/jdbc_intro";
			String user="root";
			String password="root";
			Connection connection= DriverManager.getConnection(url, user, password);
   		    String query="DELETE Student SET name=? WHERE id=?";
   		    PreparedStatement ps= connection.prepareStatement(query);
   		    ps.setString(1, query);
			
			
			ps.setInt(2, 101);
			ps.setString(2, "Ram");
			ps.setString(3, "jdbc");
			
			int res= ps.executeUpdate();
			System.out.println(res);
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
