package dynamicCRUDOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestUpdate {                                //using  dynamically

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/jdbc_intro";
			String user="root";
			String password="root";
			Connection connection= DriverManager.getConnection(url, user, password);
   		    String query="UPDATE Student SET name=? WHERE id=?";
   		    PreparedStatement ps= connection.prepareStatement(query);
   		  
   		    ps.setString(2,"ramaditya");
   		    ps.setInt(1, 101);
			int res= ps.executeUpdate();
			System.out.println(res);
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
