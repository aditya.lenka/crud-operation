package studentAPI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class StudentRepositoryImp implements StudentRepository{

	@Override
	public Connection getConnectionUtil() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/jdbc_intro";
			String user="root";
			String password="root";
			Connection connection= DriverManager.getConnection(url, user, password);
			System.out.println("connected");
			return connection;
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveStudent(Student student) {                    //insert the data in database.  
		// TODO Auto-generated method stub
		Connection connection = getConnectionUtil();
		String query="INSERT INTO Student (id,name,course) VALUES(?,?,?)";
		try {
			PreparedStatement ps= connection.prepareStatement(query);
			ps.setInt(1,student.getId());
			ps.setString(2,student.getName());
			ps.setString(3,student.getCourse());
			int res= ps.executeUpdate();
			connection.close();
			return "no. of rows added="+res;
			
		}catch(SQLException e){
			e.printStackTrace();
			
		}
		return null;
	}

	@Override
	public String updateStudentById(Student student) {
	
		Connection connection = getConnectionUtil();
     	String query="UPDATE Student SET name=?,course=?,WHERE id=? ";
		try {
			PreparedStatement ps= connection.prepareStatement(query);
			ps.setString(2,student.getName());
			ps.setString(3,student.getCourse());
			ps.setInt(3,student.getId());
			int res= ps.executeUpdate();
			connection.close();
			return "no. of rows added="+res;
			
		}catch(SQLException e){
			e.printStackTrace();
			
		}
		return null;
		
	}

	@Override
	public String deleteStudentById(int id) {
		Connection connection = getConnectionUtil();
		String query="DELETE FROM Student WHERE id=?";
		try {
			PreparedStatement ps= connection.prepareStatement(query);
			ps.setInt(1,id);
			int res= ps.executeUpdate();
			connection.close();
			return "no. of rows deleted="+res;
			
		}catch(SQLException e){
			e.printStackTrace();
			
		}
		return null;
		
	}

	@Override
	public Student getStudentById(int id) {
		Connection connection = getConnectionUtil();
		Student student= new Student();
		String query="SELECT * FROM student where id=?";
		try {
			PreparedStatement ps= connection.prepareStatement(query);
			ps.setInt(1,id);
			ResultSet res= ps.executeQuery();
			if(res.next()) {
				student.setId(res.getInt(1));
				student.setName(res.getString(2));
				student.setCourse(res.getString(3));
			}
			connection.close();
			return student;
			
		}catch(SQLException e){
			e.printStackTrace();
			
		}
		return null;

	}

	@Override
	public List<Student> getStudentDetails() {
		// TODO Auto-generated method stub
		return null;
	}

}
