package crudOperations;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class TestSelect {
	public static void main(String[] args) {
	
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url="jdbc:mysql://localhost:3306/jdbc_intro";
		FileInputStream file= new FileInputStream("mydbinfo.properties");
		Properties properties =new Properties();
		properties.load(file);
		Connection connection = DriverManager.getConnection(url,properties);
		Statement statement= connection.createStatement();
		String queary="SELECT * FROM Student";
		boolean res=statement.execute(queary);
		System.out.println(res);
		ResultSet resultset = statement.getResultSet();
		System.out.println(resultset);
		while(resultset.next()) {
			System.out.println(resultset.getInt(1));
			System.out.println(resultset.getString(2));
			System.out.println(resultset.getString(3));
			
		}
		connection.close();
		
	}catch (ClassNotFoundException | IOException | SQLException e) {
			
		e.printStackTrace();
		}

	}
}
