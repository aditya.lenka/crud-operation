package crudOperations;

import java.sql.*;

public class TestMultiInsert {
	public static void main(String[] args) throws SQLException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/jdbc_intro";
			String user="root";
			String password="root";
			Connection connection= DriverManager.getConnection(url, user, password);
			
			Statement s = connection.createStatement();
			
			s.addBatch("INSERT INTO student VALUES(2,'Aditya','java')");
			s.addBatch("INSERT INTO student VALUES(3,'raja','sql')");
			s.addBatch("INSERT INTO student VALUES(4,'rakesh','html')");
			s.addBatch("INSERT INTO student VALUES(5,'Guru','css')");
			int[] res =s.executeBatch();
		
			System.out.println(res.length);
			connection.close();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
